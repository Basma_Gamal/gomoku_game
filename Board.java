import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class Board extends Canvas implements MouseListener{



	
	Point mousePoint = new Point();
	Rules r= new Rules();
	private JFrame frame;
	Action a= new Action();
	
	
	
	
	public Point getMousePoint() {
		return mousePoint;
	}

	public void setMousePoint(Point mousePoint) {
		this.mousePoint = mousePoint;
	}

	/**
	 * Create the application.
	 */
	public Board() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
         //setBackground(new Color(87, 27, 126));
		setBackground(new Color(205, 149, 12 ));
		 addMouseListener(this);
	
	}
	
	
	  public void paint(Graphics g){
		  // Draw grid
		//  g.setColor(new Color(66, 125, 26));
		  g.setColor(new Color(0  ,	0 	,255));  
	        
	        for (int i = 16; i >= 0; i--)
	        {
				g.drawLine(0 + i * 30, 0, 0 + i * 30, 0 + 16*30);
	        }
			for (int j = 16; j >= 0; j--)
			{
				g.drawLine(0, 0 + j * 30, 0 + 16*30, 0 + j * 30);
			}
			
			
			
			// Draw pieces
			//g.setColor(Color.WHITE);
			for (int i = 15; i >= 0; i--)
				for (int j = 15; j >= 0; j--)
				{
					if(mousePoint.getX()!=0 & mousePoint.getY()!=0)
					{
						
						if(r.cells[i][j]==1){
							g.setColor(Color.BLACK);
						g.fillOval(i*30+ 4, j*30+ 4 ,30 -8, 30 - 8);
						}
						if(r.cells[i][j]==2){
							g.setColor(Color.WHITE);
						g.fillOval(i*30 + 4, j*30 + 4,30 -8, 30 - 8);
						}
					
					}
				}
		
	    }

	@Override
	public void mouseClicked(MouseEvent event) {
		// TODO Auto-generated method stub

		 mousePoint.setX(event.getX());
		 mousePoint.setY(event.getY());
	

		
		 if(r.isTurn(mousePoint)==true && r.GameOver()==false)
		 {
		if( r.isValid(mousePoint)==true)
		{
		         repaint();
				 mousePoint.setTurnpoint(mousePoint.getTurnpoint());
				 if( r.isWinner() == 1 ){
					 
					 String s=a.name;
			        
			          JOptionPane.showMessageDialog(frame, " Winner is  : " + s);
			          JOptionPane.showMessageDialog(frame,  " Game over.");
			          int confirm = JOptionPane.showConfirmDialog(frame, "Would you like play again?", "An Inane Question", JOptionPane.YES_NO_OPTION);
					  if (confirm == JOptionPane.YES_OPTION) 
					    {
						  for (int i = 0 ; i < 16 ; i++)
						    {
						    	 for (int j = 0 ; j < 16 ; j++)
						    	 {
						    		 r.cells[i][j] = 0 ;
						    	 }
						    }
						    repaint();
						  
						} 
					   else System.exit(0); 
			          
				 }
				 
				 else if( r.isWinner() == 2 ){
					  
					   String s2=a.name2;
					 
					   JOptionPane.showMessageDialog(frame, " Winner is  : " + s2);
					   JOptionPane.showMessageDialog(frame,  " Game over.");  
					   PlayAgain p = new PlayAgain(frame);
					   int confirm = JOptionPane.showConfirmDialog(frame, "Would you like play again?", "An Inane Question", JOptionPane.YES_NO_OPTION);
						  if (confirm == JOptionPane.YES_OPTION) 
						    {
							 
							  for (int i = 0 ; i < 16 ; i++)
							    {
							    	 for (int j = 0 ; j < 16 ; j++)
							    	 {
							    		 r.cells[i][j] = 0 ;
							    	 }
							    }
							    repaint();
							  
							} 
						   else System.exit(0);
				 }
				
				 
				 
		}
		else 
			JOptionPane.showMessageDialog(frame," invalid movement.");
	
		}
		 else 
			 JOptionPane.showMessageDialog(frame,  " invalid turn.");
	
		  if(r.GameOver()==true)
		  {
			  JOptionPane.showMessageDialog(frame,  " Game over.");  
			  int confirm = JOptionPane.showConfirmDialog(frame, "Would you like play again?", "An Inane Question", JOptionPane.YES_NO_OPTION);
			   if (  confirm   == JOptionPane.YES_OPTION ) 
				    {
					    for (int i = 0 ; i < 16 ; i++)
					    {
					    	 for (int j = 0 ; j < 16 ; j++)
					    	 {
					    		 r.cells[i][j] = 0 ;
					    	 }
					    }
					    repaint();
					  
					} 
				   else if (confirm == JOptionPane.NO_OPTION)  
					   System.exit(0);
			  
		  }
			 
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	


}
