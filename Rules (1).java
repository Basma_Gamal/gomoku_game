import javax.swing.JFrame;
import javax.swing.JOptionPane;


//import java.awt.Event;

public class Rules {
	static int cells[][] = new int [16][16];      // board  cells
	int turn =0;  // turn of the player = 0 as no one is playing now (initially)

	
	public int getTurn() {    //getter
		return turn;
	}
	public void setTurn(int turn) {    //setter
		this.turn = turn;
	}
	public boolean isValid( Point m) {
		
		int i = m.getX() / 30;
		int j = m.getY() / 30;
		
		if (cells[i][j] == 0)     // if is the cell is empty
		{
			if(getTurn()==0 || getTurn()==2){  //  is the cell empty or was the player 2 is turn 
			     cells[i][j]=1;     // make the cell with black piece  (make the player 1 play)
			     setTurn(1);  // make the turn to the first player
			}
			else
			 {
			if(getTurn()==1){     // if the player was turn
			     cells[i][j]=2; // make the cell 2 (let player 2 turn)
			     setTurn(2);//make the turn to the player 2
			 }
			}
			return true;  // succeeded

		}
		return false;  // otherwise


	}
	
	
	public boolean isTurn( Point m) {

		if (m.getTurnpoint() == getTurn() )     // if the point(cell) = the getTurn(the number inside the cell)
		{
			return true;     
		}
		return false;


	}
	public void showcells()     // print the board cells in the console
	{
		for (int i=0;i<16;i++)
		{
			for (int j=0;j<16;j++)
				System.out.print(cells[i][j] + " ");
			System.out.println("     ");
		}
	}
		
	int isWinner()  // check for the winner
	{
		int count1; 
		int count2;  ////// check ROWS
	for( int i = 0; i < 16; i++ )
	{ count1 = 0; count2 = 0; 
	 for(int j = 0; j < 16; j++ )
	  {
		 if( cells[i][j] == 1 ) // is the black
	     {
			 count1++;  // increase by one
			 if( count1 == 5 ) // if it reaches 5 pieces horizontally
			 {
				 return 1; //return 1 as the first player is the winner
			 }
			 count2 = 0;   // make the counter of the second player = 0
		} 
	else
		if( cells[i][j] == 2 )// player 2
		{
			count2++;
			if( count2 == 5 )
			{
				return 2; 
			}
			count1 = 0;
		}
	 else 
	     {
		 count1 = 0;  // set counter 1 & 2 = 0 
		 count2 = 0;
	     }
	  }
	}
		 ////// Check Columns 
		for( int j = 0; j < 16; j++ ) 
		{
			count1 = 0;
			count2 = 0; 
			for(int i = 0; i < 16; i++ )
		    { 
				if(cells[i][j] == 1 )
				{
					count1++;
					if( count1 == 5 )
					{
						return 1; 
					}
				   count2 = 0;
				 } 
		       else 
		    	if( cells[i][j] == 2 )
		    	{
		    		count2++;
		    		if( count2 == 5 )
		    		{
		    			return 2; 
		    		}
		    		count1 = 0; 
		    	} 
		        else 
		        {
		        	count1 = 0; 
		        	count2 = 0;
		        }
			}
		}
	///////Check Diagonals Right Direction 
			for( int i = 0; i < 16; i++ )
			{
				for( int j = 0; j < 16; j++ )
				{
					count1 = 0; count2 = 0;
			        for( int x = i, y = j; x < 16 && y < 16; x++, y++ ) // loop with 1 and the second 2 and the third 3 and so on
			        	//left
			        { 
			        	if( cells[x][y] == 1 ) 
			        	{
			        		count1++;
			        		if( count1 == 5 )
			        		{
			        			return 1;
			        		}
			               count2 = 0;
			             } 
			            else
			            if( cells[x][y] == 2 ) 
			            {
			            	count2++;
			            	if( count2 == 5 )
			                {
			            		return 2; 
			            	} count1 = 0; 
			            } 
			            else 
			            {
			            	count1 = 0; 
			            	count2 = 0; 
			            }
			        }
			   	} 
			}
			/////////// Check Diagonal Left direction 
			 for( int i = 0; i < 16; i++ ) 
			 {
				 for( int j = 0; j < 16; j++ ) 
			     {
					 count1 = 0; 
					 count2 = 0;
			       for( int x = i, y = j; x <16 && y >= 0; x++, y-- )  
			      {// loop with 1 and the second 2 and the third 3 and so on
			        	//left
			    	   if( cells[x][y] == 1 ) 
			    	   {
			    		   count1++; 
			              if( count1 == 5 ) 
			              {
			            	  return 1;
			              }
			              count2 = 0; 
			           } 
			          else 
			          if(cells[x][y] == 2 ) 
			          {
			        	  count2++; 
			        	  if( count2 == 5 )
			              {
			        		  return 2; 
			        	  }
			        	  count1 = 0; 
			          }
			          else 
			          {
			        	  count1 = 0; 
			        	  count2 = 0; 
			          }
			    	}
			     }
			  }
			 return 0; 
    }
	
	
	public boolean GameOver()
	{
		int count = 0 ;
		for (int x = 0; x < 16; x++)
		{
			for (int y = 0; y < 16; y++) 
			{
				if(cells[x][y]== 1 || cells[x][y]==2)  // check the cells all is empty with player 1 and 2 moves
				{
					if(isWinner() ==  0) // and no one wins
				   {
						 count++;// increase the counter
					
				   }
				
			    }
			}
	     } 
	             if (count == 256 ) // the cells size (16*16)  all are full
	             {
	            	 return true;
	             }
	             else 
	            	 return false;
     }
	
}


	

