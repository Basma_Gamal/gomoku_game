public class Point {

	int x ;
	int y ;
	int turnpoint=0;
	
	public int getTurnpoint() {
		return turnpoint;
	}

	public void setTurnpoint(int turnpoint) {
		if(turnpoint==0 || turnpoint==2)
		  this.turnpoint = 1;
		if(turnpoint==1)
			this.turnpoint = 2;
	}

	public Point() {
		
	}

	public Point(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}


	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

}
