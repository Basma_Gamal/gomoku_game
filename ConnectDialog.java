
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.*;

class ConnectDialog extends JDialog implements ActionListener {

  private JTextField username = new JTextField("");


  private boolean okPressed;

  private JButton okButton;

  private JButton cancelButton;

  public ConnectDialog(JFrame parent) {
	  
    super(parent, "Player Name", true);
    Container contentPane = getContentPane();
    JPanel p1 = new JPanel(new GridLayout(2, 2,3,3));
    p1.add(new JLabel("Player name:"));
    p1.add(username);
    contentPane.add("Center", p1);

    Panel p2 = new Panel();
    okButton = addButton(p2, "Ok");
    cancelButton = addButton(p2, "Cancel");
    contentPane.add("South", p2);
    setSize(240, 120);
  }

  private JButton addButton(Container c, String name) {
    JButton button = new JButton(name);
    button.addActionListener(this);
    c.add(button);
    return button;
  }

  public void actionPerformed(ActionEvent evt) {
    Object source = evt.getSource();
    if (source == okButton) {
      okPressed = true;
      setVisible(false);
    } else if (source == cancelButton)
      setVisible(false);
  }

  public boolean showDialog(UserInfo transfer) {
    username.setText(transfer.username);
    okPressed = false;
    show();
    if (okPressed) {
      transfer.username = username.getText();

    }
    return okPressed;
  }
}



