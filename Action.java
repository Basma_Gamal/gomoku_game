import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class Action implements ActionListener {

	private JMenuItem mItem;
	private JFrame aframe;
	private JFrame aframe2;
	public static String name,name2 ;
	
	public Action(JMenuItem i ,JFrame f) {
		 mItem=i;
		 aframe=f;
		 aframe2=f;
	}
	
	public Action(JMenuItem i ) {
		 mItem=i;
	}
	
	public Action() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub
		
		   if(mItem.getText()== "New Game")
		   { 
			  
			  
			      Board newBoard =new Board();
			     
				   ConnectDialog  dialog = new ConnectDialog(aframe);
				   //ConnectDialog  dialog2 = new ConnectDialog(aframe2);
			      UserInfo transfer = new UserInfo("", "");
			      UserInfo transfer2 = new UserInfo("", "");
				  
				 if (dialog.showDialog(transfer)) {
				  name = transfer.username;
				  
				 }
				 if (dialog.showDialog(transfer2)) {
					  name2 = transfer2.username;
					  
					 }
				 
			    JPanel playerPanel =new JPanel();
				JLabel playerLabel = new JLabel("");
				JPanel playerPanel2 =new JPanel();
				JLabel playerLabel2 = new JLabel("");
				playerLabel.setText(name);
				playerPanel.add(playerLabel);
				Container cp = aframe.getContentPane();
			    cp.setLayout(new BorderLayout());
			    cp.add(newBoard, BorderLayout.CENTER);
			    cp.add(playerPanel , BorderLayout.EAST);
			    cp.add(playerPanel2 , BorderLayout.WEST);
			    aframe.setContentPane(cp);
			    aframe.setVisible(true);
			    ////////////////////////////
				playerLabel2.setText(name2);
				playerPanel2.add(playerLabel2);
			    aframe2.setContentPane(cp);
			    aframe2.setVisible(true);
			    
			    
			   } 
		   
		   
		   if(mItem.getText()== "Help Contents")
		   {
			   HelpContents help = new HelpContents();
		   }
		   
		   if(mItem.getText()== "About Gomoku")
		   {
			   AboutGomoku aboutGomoku = new AboutGomoku();
		   }
		   
		   if(mItem.getText()== "Exit")
		   {
			   System.exit(0);
		   }
		
		   
	}
}

	