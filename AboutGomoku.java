import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JTextPane;


public class AboutGomoku {

	private JFrame aboutFrame;
	/**
	 * Create the application.
	 */
	public AboutGomoku() {
		aboutInitialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void aboutInitialize() {
		aboutFrame = new JFrame(); // frame of about gomoku
		aboutFrame.setBounds(100, 50, 300, 200); // set size 
		aboutFrame.setTitle("About Gomoku"); // the name of the frame
		aboutFrame.setVisible(true);  // make it visible
		
		String about="   Version: Gomoku Release 1 \n" +  // the text inside the frame
		
                 "   Build id: 20130919-0819\n "  +
                 "  (c) Copyright Gomoku Team  All rights reserved. \n";
		JTextPane textPane = new JTextPane();  // make a text pane
		textPane.setText(about);  // add the string into the text pane
		textPane.setContentType("text/plain"); // type of the conent
		textPane.setEditable(false); 
		aboutFrame.getContentPane().add(textPane,BorderLayout.CENTER);    
	
	}

}


