
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;


import javax.swing.JSeparator;


public class GomokuGui {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GomokuGui window = new GomokuGui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		

		
	}

	/**
	 * Create the application.
	 */
	public GomokuGui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(300, 100, 570, 545);
		frame.setTitle("Gomoku");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnMainMenu = new JMenu("Main Menu");
		menuBar.add(mnMainMenu);
		
		JMenuItem mntmNewGame = new JMenuItem("New Game");
		mnMainMenu.add(mntmNewGame);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mnMainMenu.add(mntmExit);
		
		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		JMenuItem mntmHelpContents = new JMenuItem("Help Contents");
		mnHelp.add(mntmHelpContents);
		
		JSeparator separator = new JSeparator();
		mnHelp.add(separator);
		
		JMenuItem menuItem = new JMenuItem("About Gomoku");
		mnHelp.add(menuItem);
		
	
	
		
		mntmExit.addActionListener(new Action(mntmExit));
		mntmHelpContents.addActionListener(new Action(mntmHelpContents));
		menuItem.addActionListener(new Action(menuItem));
		
		
		mntmNewGame.addActionListener(new Action(mntmNewGame,frame));

		Board b =new Board();
		
		
	}

}

